# Utilisez une image de base Node.js
FROM node:latest

# Définissez le répertoire de travail dans le conteneur
WORKDIR /project

# Copiez les fichiers package.json et package-lock.json dans le répertoire de travail
COPY package*.json ./

# Installez les dépendances
RUN npm install

# Copiez le reste des fichiers du projet dans le répertoire de travail
COPY . .

# Commande par défaut pour démarrer le conteneur
CMD ["tail", "-f", "/dev/null"]
