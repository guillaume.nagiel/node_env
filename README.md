# Environnement de Développement NodeJS

## Description du projet

### Introduction
Ce dépot va vous permettre de déployer un environnement de développement en NodeJs. Une fois démarré, vous pourrez commencer à coder.

### Contenu
Dans ce container, vous trouverez des images de :
- [node:latest](https://hub.docker.com/_/node).

## Prérequis
Pour utiliser ce container de manière simple, télécharger et lancer le logiciel [Docker Desktop](https://docs.docker.com/get-docker/)

## Installation

### Installation du container
Une fois `Docker Desktop` installé, vous pouvez cloner ce dépot.
```
git clone https://gitlab.com/guillaume.nagiel/node_env.git
```

Puis positionnez-vous dans ce dépot et créez l'image docker.
```
cd node_env
```

> :warning: **Cette suite de commandes sera à répéter à chaque lancement du conteneur**

Création et lancement du conteneur
```
docker-compose up --build
```

### Commande pour ouvrir un shell:


Entrer dans le `shell`:
```
docker-compose exec nodejs bash
```

Une fois dans ce `shell`, positionnez vous dans le dossier `project` :
```
cd project
```

Vous pouvez maintenant lancer n'importe quelle script présent dans le dossier `project` avec la commande :
```
node <monScript.js>
```

Pour sortir de ce `shell`, saisissez :
```
exit
```

Bon développement :sunglasses:

# Liste des fonctionnalités à ajouter

### Todo

### In Progress

### Done ✓

- [x] Créer le README.md  
- [x] Créer le TODO.md  